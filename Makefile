
# ==========================
# Alt-Tab
# ==========================

# Exports the plist for Alt-Tab.
.PHONY: export-config-alt-tab
export-config-alt-tab:
	defaults export com.lwouis.alt-tab-macos ./alt-tab/com.lwouis.alt-tab.macos.plist

# Imports the plist for Alt-Tab.
.PHONY: import-config-alt-tab
import-config-alt-tab:
	defaults import com.lwouis.alt-tab-macos ./alt-tab/com.lwouis.alt-tab.macos.plist

# ==========================
# Karabiner
# ==========================

# Imports the configuration file for Karabiner.
.PHONY: import-config-karabiner
import-config-karabiner:
	mkdir -p ~/.config/karabiner
	cp ./karabiner/karabiner.json ~/.config/karabiner

# ==========================
# Amethyst
# ==========================

# Exports the plist for Amethyst.
.PHONY: export-config-amethyst
export-config-amethyst:
	defaults export com.amethyst.Amethyst.plist ./amethyst/com.amethyst.Amethyst.plist

# Imports the plist for Amethyst.
.PHONY: import-config-amethyst
import-config-amethyst:
	defaults import com.amethyst.Amethyst.plist ./amethyst/com.amethyst.Amethyst.plist
